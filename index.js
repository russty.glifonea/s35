const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")

dotenv.config()

const app = express()
const port = 3001


// MongoDB Connection
mongoose.connect(`mongodb+srv://Tateng501152:${process.env.MONGODB_PASSWORD}@cluster0.in9k1rn.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))
// MongoDB Connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})
// MongoDB Schemas End

// MongoDB Model 
const Task = mongoose.model('Task', taskSchema)
// MongoDB Model END

// Routes
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask) => {
			if(error){
				return consile.error(error)
			}
			else {
				return response.status(200).send('New task created!')
			}
		})
	})
})

app.get('/tasks', (request, response) => {
	Task.find({}, (error, result) => {
		if(error) {
			return console.log(error)
		}
		return response.status(200).json({
			data: result
		})
	})
})


// Routes END



// MongoDB Schemas
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: 'Pending'
	}
})
// MongoDB Schemas End

// MongoDB Model 
const User = mongoose.model('User', taskSchema)
// MongoDB Model END

// Routes
app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send('Duplicate user found!')
		}

		let newUser = new User({
			user: request.body.name,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if(error){
				return consile.error(error)
			}
			else {
				return response.status(200).send('New user created!')
			}
		})
	})
})

// Routes END
app.listen(port, () => console.log(`Server running at localhost:${port}`))